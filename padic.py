#!python3
"""Rekenmachine voor p-adische getallen en repeterende breuken.
"""

"""Syntax:
+-*/ werkt als verwacht
-/ ook als prefix operator (/n = 1/n)
^ is macht
Getallen kan je schrijven als:
'. geeft "quote notatie", b.v. 285714'3 = 1/7 (impliciete punt aan eind)
.r geeft "repeterende notatie", b.v. .1r6 = 1/6 (impliciete punt aan begin)
! wordt geinterpreteerd als '.

Leuke weetjes:
Lengte repeterend deel versus noemer:
1     3, 9
2     11
3     27, 37
5     41
6     7, 13
7     239
8     73
"""


#TODO 2: beter testen andere talstelsels

from fractions import Fraction as Breuk
from operator import add, sub, mul, truediv
from decimal import Decimal, getcontext
from collections import defaultdict
from itertools import starmap
from functools import reduce
from math import sqrt

try:
    from math import gcd
except ImportError:
    from fractions import gcd

#constantes---------------------------------------
OK = 'ok'
EINDE = 'einde'
#dit is het grondtal; het programma werkt ook voor andere grondtallen
#maar de testen zullen falen
GRONDTAL = 10
ALLECIJFERS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
#maximum aantal cijfers om te berekenen
MAXBREEDTE = 10000
#extra compact output (! voor p-adisch en impliciete , voor repeterend)
COMPACT = True

class InternalError(Exception): pass
#handige functies---------------------------------

def vindWijzers(s):
    """Split string s in "cijfers" en "wijzers"
    geef een positie van alle niet-cijfers in de string,
    waarbij positie is gedefinieerd als "het aantal cijfers ervoor"
    >>> vindWijzers('50.4r78') == ('50478', {'.': 2, 'r': 3})
    True
    """
    pos = 0
    wijzers = {}
    cijfers = ''
    for c in s:
        if c.isdigit():
            cijfers += c
            pos +=1
        else:
            wijzers[c] = pos
    return cijfers, wijzers

def plaatsWijzers(cijfers, wijzers):
    """Omgekeerde van vindWijzers: plaats de wijzers terug.
    >>> plaatsWijzers('123456', {'.': 2, 'r': 3})
    '12.3r456'
    """
    def itemKey(wi):
        #zet wijzers in de juiste volgorde
        wijzer, pos = wi
        return pos, "-',r".find(wijzer)
    resultaat = ''
    lengte = 0
    for wijzer,pos in sorted(wijzers.items(), key=itemKey):
        if lengte<pos:
            resultaat += cijfers[lengte:pos]
            lengte = pos
        resultaat += wijzer
    assert lengte<=len(cijfers), "getal te kort"
    return resultaat+cijfers[lengte:]

def splits(s, pos):
    """Splits s op plaats pos, en maak er getallen van
    >>> splits('12345', 3)
    (123, 45)
    """
    return int(s[:pos] or '0', GRONDTAL), int(s[pos:] or '0', GRONDTAL)

def talstelsel(x, d, expandeer=False):
    """Schrijf x in talstelsel met d cijfers
    >>> talstelsel(10, 3)
    '010'
    >>> talstelsel(0, 0)
    ''
    >>> talstelsel(4, 2, True)
    '04'
    """
    resultaat = ''
    while expandeer and x or d>0:
        resultaat = ALLECIJFERS[x%GRONDTAL]+resultaat
        x //= GRONDTAL
        d = d-1
    assert expandeer or x==0
    return resultaat

def benaderPadisch(s):
    """Vind een breuk met p-adische benadering s.
    Bekend als Wang's ratconvert algoritme.
    Lees "Efficient Rational Number Reconstruction" van Collins en Encarnacion
    om te zien waarom dit niet perfect werkt.
    Ik gebruik hun notatie.
    >>> benaderPadisch('00353513035793194874060989')
    Fraction(18107, 2263)
    """
    #TODO BUG: if you enter ~6, you do not get -15/10, but an error instead
    cijfers, wijzers = vindWijzers(s)
    u, m = int(cijfers), GRONDTAL**len(cijfers)
    a1, a2, v1, v2 = m, u, 0, 1
    grens = sqrt(m/2)
    while True:
        if abs(v2)>=grens:
            raise ValueError("~"+s+": geen unieke benadering")
        if a2<grens:
            if abs(gcd(a2,v2))!=1:
                raise ValueError("~"+s+": geen benadering")
            schuif = len(cijfers)-wijzers.get('.', len(cijfers))
            return Breuk(a2, v2*GRONDTAL**schuif)
        q = a1//a2
        a1, a2, v1, v2 = a2, a1-q*a2, v2, v1-q*v2

def benaderBreuk(s, marge=None):
    """Benader een positief getal gegeven in een string als een breuk.
    Zoek de eenvoudigste breuk die afgerond het getal oplevert.
    als marge is ingesteld, geef het aantal kleinste eenheden die je mag afwijken
    >>> benaderBreuk('2.2037')
    Fraction(119, 54)
    >>> benaderBreuk('3.1415926', 4)
    Fraction(355, 113)
    """
    rest = Breuk(s)
    cijfersAchterKomma = len(s)-s.index('.')-1
    ulp = Breuk(1, GRONDTAL**cijfersAchterKomma)
    if marge is None: afwijking = ulp/2
    else: afwijking = ulp*marge
    ondergrens, bovengrens = rest-afwijking, rest+afwijking
    #doe de kettingbreukbenadering: begin met 1/0, q/1
    q = int(rest)
    t1, n1, t2, n2 = 1, 0, q, 1
    try:
        # q = int(rest) at this point
        while not ondergrens<Breuk(t2, n2)<=bovengrens:
            rest = 1/(rest-q)
            q = int(rest)
            t1, n1, t2, n2 = t2, n2, q*t2+t1, q*n2+n1
    except ZeroDivisionError:
        raise ValueError("~"+s+": geen benadering")
    while ondergrens<Breuk(t2-t1, n2-n1)<=bovengrens:
        t2, n2 = t2-t1, n2-n1
    return Breuk(t2,n2)

#de parser-------------------------------------------
CIJFERS = ALLECIJFERS[:GRONDTAL]
GETALBEGINTEKENS = CIJFERS+'.,r~'
GETALTEKENS = GETALBEGINTEKENS+"'!"
PREFIXTEKENS = GETALBEGINTEKENS+'-/'
SOMTEKENS = '+-'
PRODTEKENS = '*/%'
MACHTTEKENS = '^'
LAATSTE = '_'

class StateMaker(type):
    """Metaclass voor State.
    Als je een state tabel meegeeft, wordt de teken() functie
    niet meer aangeroepen.
    """
    #TODO: gebruik deze metaclass voor een tabel. Kan alleen met python3
    #TODO: doe dit met tabel. Invoer: class, substate. Mogelijke waarden: None, accepteer, bindRechts(State), bindLinks, EINDE. Probleem: Getal moet apart
    def __new__(cls, naam, bases, ruimte):
        if 'parseTabel' in ruimte:
            #expandeer
            #actie is hier None, OK, EINDE, <State klasse>, Dyadisch.bindLinks, Dyadisch.bindRechts(State klasse)
            #TODO: idee: actie is element uit de betreffende klasse (die je er met __self__ weer uit peutert)
            ruimte.parseTable = {
                (teken, sub): actie
                for (tekens, subs), actie in ruimte.parseTabel.items()
                for teken in tekens
                for sub in subs
                }
        return type.__new__(cls, naam, bases, ruimte)

class State:
    """Een state van de parser state machine,
    Dit is een gedeeltelijk verwerkte subexpressie.
    """

    def __init__(self, *inhoud):
        #Inhoud bevat geaccepteerde tekens en subexpressies in volgorde
        self.inhoud = list(inhoud)

    def __repr__(self):
        return self.__class__.__name__+repr(self.inhoud)

    def accepteer(self, c):
        """Sla op in inhoud,
        zodat teken later verwerkt kan worden"""
        self.inhoud.append(c)
        return OK

    def teken(self, c):
        """Verwerk teken c, geef terug:
        None: Syntaxfout
        OK: teken is verwerkt
        EINDE: einde van deze subexpressie
        Expressie: eerst komt deze subexpressie
        Roept normaal teken0, teken1, aan, bepaald door lengte inhoud.
        (Zou ook met tabel kunnen, trouwens, de routine doet niet veel.)
        Als de functie er niet is, komt er een syntax error.
        """
        routine = 'teken'+str(len(self.inhoud))
        try:
            return getattr(self, routine)(c)
        except AttributeError: return None

    def uitkomst(self, extra=None):
        """De uitkomst van de expressie,
        roept uitkomst1, uitkomst2, enz. aan,
        met de inhoud als argumenten
        extra wordt aan inhoud toegevoegd (voor einde).
        """
        inhoud = self.inhoud
        if extra is not None: inhoud = inhoud+[extra]
        routine = 'uitkomst'+str(len(inhoud))
        try:
            return getattr(self, routine)(*inhoud)
        except AttributeError: return None

    def gphl(self, c):
        """Start van een getal, prefix of haakjes.
        Wordt ook gebruikt als teken2 en als Prefix.teken1
        """
        if c in GETALBEGINTEKENS: return Getal()
        if c in PREFIXTEKENS: return Prefix()
        if c=='(': return Haakjes()
        if c==LAATSTE: return Laatste()

class Dyadisch(State):
    """Gemeenschappelijk voor dyadische operatoren"""
    def uitkomst1(self, expr):
        return expr

    teken0 = teken2 = State.gphl

    def bindLinks(self, c):
        "Als het volgende teken aangeeft dat we links recursief moeten gaan"
        self.inhoud = [self.uitkomst(), c]
        return OK

    def bindRechts(self, stateClass):
        "Begin nieuwe subexpressie met laatste subexpressie"
        return stateClass(self.inhoud.pop())

class Som(Dyadisch):
    """Som of verschil, links recursief
    >>> p = Parser(); p.verwerkString('4+5-')
    >>> p.stapel
    [Som[Fraction(9, 1), '-']]
    >>> p.verwerkString('6/')
    >>> p.stapel
    [Som[Fraction(9, 1), '-'], Product[Fraction(6, 1), '/']]
    """
    def teken1(self, c):
        if c in SOMTEKENS: return self.accepteer(c)
        if c in PRODTEKENS: return self.bindRechts(Product)
        if c in MACHTTEKENS: return self.bindRechts(Macht)
        if c==')': return EINDE

    def teken3(self, c):
        if c in SOMTEKENS: return self.bindLinks(c)
        if c in PRODTEKENS: return self.bindRechts(Product)
        if c in MACHTTEKENS: return self.bindRechts(Macht)
        if c==')': return EINDE

    def uitkomst3(self, expr1, op, expr2):
        if op=='+': return expr1+expr2
        if op=='-': return expr1-expr2

class Product(Dyadisch):
    """Product of quotient, links recursief
    >>> p = Parser(); p.verwerkString('2*3^')
    >>> p.stapel
    [Som[], Product[Fraction(2, 1), '*'], Macht[Fraction(3, 1), '^']]
    """
    def teken1(self, c):
        if c in SOMTEKENS: return EINDE
        if c in PRODTEKENS: return self.accepteer(c)
        if c in MACHTTEKENS: return self.bindRechts(Macht)
        if c==')': return EINDE

    def teken3(self, c):
        if c in SOMTEKENS: return EINDE
        if c in PRODTEKENS: return self.bindLinks(c)
        if c in MACHTTEKENS: return self.bindRechts(Macht)
        if c==')': return EINDE

    def uitkomst3(self, expr1, op, expr2):
        if op=='*': return expr1*expr2
        if op=='/': return expr1/expr2
        if op=='%': return expr1%expr2

class Macht(Dyadisch):
    """Macht, rechts recursief.
    >>> p = Parser(); p.verwerkString('2^3^')
    >>> p.stapel
    [Som[], Macht[Fraction(2, 1), '^'], Macht[Fraction(3, 1), '^']]
    """
    def teken1(self, c):
        if c in SOMTEKENS or c in PRODTEKENS: return EINDE
        if c in MACHTTEKENS: return self.accepteer(c)
        if c==')': return EINDE

    def teken3(self, c):
        if c in SOMTEKENS or c in PRODTEKENS: return EINDE
        if c in MACHTTEKENS: return self.bindRechts(Macht)
        if c==')': return EINDE

    def uitkomst3(self, expr1, op, expr2):
        """Bereken a**b**c**..."""
        return expr1**expr2

class Prefix(State):
    """- of /
    >>> p = Parser(); p.verwerk('-')
    >>> p.stapel
    [Som[], Prefix['-']]
    >>> p.verwerk('('); p.stapel
    [Som[], Prefix['-'], Haakjes['(']]
    >>> bereken("1-/3")
    Fraction(2, 3)
    """
    teken0 = State.accepteer

    teken1 = State.gphl

    def teken2(self, c): return EINDE

    def uitkomst2(self, op, waarde):
        if op=='-': return -waarde
        if op=='/': return 1/waarde

class Getal(State):
    """Getal.
    ' mag worden gebruik voor p-adisch, bv ,3'5 = 1/60
    r mag worden gebruikt voor repetent, bv ,r3 = 1/3
    ~ mag worden gebruikt voor benaderen, bv 2,2037~ = 119/54
    (dit betekent: simpelste breuk die afgerond 2,2037 is)
    ~ aan het begin maakt een p-adische benadering

    , wordt gelezen als . en ! wordt gelezen als '.
    >>> bereken('34.5')
    Fraction(69, 2)
    >>> bereken('r142857')
    Fraction(1, 7)
    >>> bereken("285714'3")
    Fraction(1, 7)
    >>> bereken('3,141593~')
    Fraction(355, 113)
    >>> bereken('~3.4')
    Fraction(1, 15)
    """
    def teken(self, c):
        inhoud = self.inhoud
        #einde van getal
        if c not in GETALTEKENS: return EINDE
        #vervang , en !
        if c==',': c = '.'
        elif c=='!':
            #expandeer
            self.accepteer("'")
            c = "."
        self.accepteer(c)
        return self.check()

    def check(self):
        """Controleer of een niet-leeg incompleet getal aan eisen voldoet,
        en geef OK als alles in orde is, anders None.
        Eisen:
        van . en , mogen er maximaal 1
        van r, ' en ~ ook
        minimaal 1 cijfer voor ', na r
        ~ aan begin (zonder .),
        of aan eind (met .), eventueel gevolgd door cijfer
        """
        inhoud = self.inhoud
        #meerdere punten, meer dan 1 typeindicator
        if inhoud.count('.')>1 \
        or inhoud.count("'")+inhoud.count('r')+inhoud.count('~')>1:
            return None
        if (
            #geen benadering, of p-adische benadering
            '~' not in inhoud[1:]
            #rationele benadering: punt verplicht
            or '.' in inhoud and (
                #~ aan het eind
                inhoud[-1]=='~' or
                #of met een (decimaal!) >0 cijfer erachter
                inhoud[-2]=='~' and inhoud[-1] in '123456789'
            )   ): return OK
        #niet goed
        return None

    def uitkomst(self, extra=None):
        """Bepaal de waarde van een getal.
        Inhoud bestaat uit een aantal strings, de cijfers zijn gegroepeerd.
        Stuurt ValueError als er iets mis is.
        >>> bereken('3.1415926~4')
        Fraction(355, 113)
        """
        inhoud = ''.join(self.inhoud)
        if '~' in inhoud:
            #maak benadering van breuk
            if inhoud[0]=='~':
                if len(inhoud)<2: raise ValueError("Losse ~ zonder cijfers")
                return benaderPadisch(inhoud[1:])
            #~ is op een van laatste posities, is getest door check
            if inhoud[-1]=='~': return benaderBreuk(inhoud[:-1])
            return benaderBreuk(inhoud[:-2], int(inhoud[-1]))
        assert extra is None
        cijfers, wijzers = vindWijzers(''.join(self.inhoud))
        #er moeten cijfers in staan, en 'r' mag niet achteraan
        #rest is al gecheckt
        if not cijfers or wijzers.get('r', 0)==len(cijfers):
            raise ValueError(inhoud+": geen getal")
        lenCijfers = len(cijfers)
        #bepaal waarde
        if "'" in wijzers:
            #p-adisch
            wijzer = wijzers["'"]
            waarde1, waarde2 = splits(cijfers, wijzer)
            waarde1 = Breuk(waarde1*GRONDTAL**(lenCijfers-wijzer),
                1-GRONDTAL**wijzer)
            waarde = waarde1+waarde2
            #schuif punt op (impliciete punt aan eind)
            waarde /= GRONDTAL**(lenCijfers-wijzers.get('.', lenCijfers))
        elif 'r' in wijzers:
            #repeterend
            wijzer = wijzers['r']
            waarde1, waarde2 = splits(cijfers, wijzer)
            waarde2 = Breuk(waarde2, (GRONDTAL**(lenCijfers-wijzer)-1))
            #schuif punt op van positie 'r' naar '.' (impliciet aan begin)
            waarde = waarde1+waarde2
            waarde *= Breuk(GRONDTAL)**(wijzers.get('.', 0)-wijzer)
        else:
            waarde = Breuk(int(cijfers, GRONDTAL),
                GRONDTAL**(lenCijfers-wijzers.get('.', lenCijfers)))
        return waarde

class Laatste(State):
    """_, dat staat voor laatste resultaat.
    """
    waarde = 0
    def teken0(self, c):
        if c=='_': return self.accepteer(c)

    def teken1(self, c):
        return EINDE

    def uitkomst(self, extra=None):
        assert extra is None
        return self.waarde

class Haakjes(State):
    """Haakjes.
    >>> p = Parser(); p.verwerkString('3/(2+5')
    >>> p.stapel
    [Som[], Product[Fraction(3, 1), '/'], Haakjes['('], Som[Fraction(2, 1), '+'], Getal['5']]
    >>> p.uitkomst()
    Fraction(3, 7)
    """
    def teken0(self, c):
        if c=='(': return self.accepteer(c)

    def teken1(self, c): return Som()

    def teken2(self, c):
        if c==')': return self.accepteer(c)

    def teken3(self, c): return EINDE

    def uitkomst2(self, h1, expr, h2=')'):
        """Zorgt ook voor het automatisch sluiten van haakjes
        >>> p = Parser(); p.verwerkString('4/(3+7')
        >>> p.uitkomst()
        Fraction(2, 5)
        >>> p.verwerkString('r,3')
        >>> p.uitkomst()
        Fraction(12, 31)
        >>> p.verwerkString(')')
        >>> p.uitkomst()
        Fraction(12, 31)
        >>> p.verwerkString(')') # doctest: +IGNORE_EXCEPTION_DETAIL
        Traceback (most recent call last):
            ...
        SyntaxError: [Expressie[Fraction(12, 31)]] ^ )
        """
        return expr

    uitkomst3 = uitkomst2

class Parser:
    """Parse een expressie, en bereken het resultaat.
    >>> p = Parser(); p.verwerkString('4+5')
    >>> p.uitkomst()
    Fraction(9, 1)
    >>> p.verwerkString('*,5')
    >>> p.stapel
    [Som[Fraction(4, 1), '+'], Product[Fraction(5, 1), '*'], Getal['.', '5']]
    >>> p.uitkomst()
    Fraction(13, 2)
    """
    def __init__(self):
        self.stapel = [Som()]

    def verwerk(self, c):
        """Verwerk teken c"""
        while True:
            #TODO: verwerk tabel in plaats van routine aanroepen
            t = self.stapel[-1].teken(c)
            if t is None:
                raise SyntaxError(str(self.stapel)+' ^ '+c)
            elif t is OK:
                #teken is verwerkt
                break
            elif isinstance(t, State):
                #ga verder met subexpressie
                self.stapel.append(t)
            elif t is EINDE:
                #einde subexpressie: sla op in volgende niveau
                #geef volgende niveau resultaat door
                s = self.stapel.pop().uitkomst()
                if not self.stapel: raise SyntaxError(str(self.stapel)+' ^ '+c)
                self.stapel[-1].inhoud.append(s)
            else:
                #routine doet het niet
                raise InternalError

    def verwerkString(self, s):
        """Handig voor debug"""
        for i,c in enumerate(s):
            try:
                self.verwerk(c)
            except SyntaxError:
                raise SyntaxError(s+'\n'+' '*i+'^')

    def uitkomst(self):
        resultaat = None
        for expr in reversed(self.stapel):
            resultaat = expr.uitkomst(resultaat)
            if resultaat is None: raise ValueError('Geen uitkomst')
        return resultaat

def bereken(expr):
    """parse een expressie, en bereken meteen het resultaat
    >>> bereken('1/3+,1r6/2,5')
    Fraction(2, 5)
    """
    parser = Parser()
    for c in expr:
        parser.verwerk(c)
    return parser.uitkomst()

#---------------simpele versie van quote en repeterend
def maakInvTabel(n):
    """Maak een inverse tabel van n modulo GRONDTAL
    """
    resultaat = [None]*GRONDTAL
    for i in range(GRONDTAL): resultaat[i*n%GRONDTAL] = i
    return resultaat

def quote2(f, maxbreedte=None, compact=False):
    """Schrijf f in quote notatie, op de naieve manier.
    >>> quote2(Breuk(1,3))
    "6'7"
    >>> quote2(Breuk(1,4))
    ',25'
    >>> quote2(Breuk(13,22))
    "09'1,5"
    """
    if maxbreedte is None: maxbreedte = MAXBREEDTE
    #zorg dat de komma aan het eind staat
    schuif = 0
    while gcd(f.denominator,GRONDTAL)>1:
        f *= GRONDTAL
        schuif +=1
    wijzers = {}
    t, n = f.numerator, f.denominator
    #bepaal de cijfers van achter naar voren
    cijfers = ''
    bekendeResten = {}
    deelModGRONDTAL = maakInvTabel(n)
    while t and len(cijfers)<=maxbreedte:
        q = deelModGRONDTAL[t%GRONDTAL]
        cijfers = ALLECIJFERS[q]+cijfers
        if t in bekendeResten:
            #we zijn rond: zet de quote goed
            repLen = len(cijfers)-bekendeResten[t]
            #kort het vaste deel in als repeterende cijfers gelijk zijn
            while len(cijfers)>repLen and cijfers[0]==cijfers[repLen]:
                cijfers = cijfers[1:]
            #zet het repeteersymbool op de goede plaats
            wijzers["'"] = repLen
            break
        #als er nog ruimte is: onthou rest voor rep detectie
        if len(cijfers)<=maxbreedte: bekendeResten[t] = len(cijfers)
        t = (t-q*n)//GRONDTAL
    #TODO: wat als we uit de maxbreedte lopen?
    if schuif: wijzers[','] = len(cijfers)-schuif
    resultaat = plaatsWijzers(cijfers, wijzers)
    #gebruik ! bij compact
    if compact: resultaat = resultaat.replace("',", '!')
    if len(resultaat)<=maxbreedte: return resultaat
    return '...'+resultaat[-maxbreedte+3:]

def repeterend2(f, maxbreedte=None, compact=False):
    """Schrijf f als repeterende breuk, op de naieve manier.
    Zet maxbreedte op het maximaal aantal tekens uitvoer dat je wil.
    >>> repeterend(Breuk(1,3))
    ',r3'
    >>> repeterend(Breuk(-100,3))
    '-3r3,'
    >>> repeterend(Breuk(-1,40))
    '-,025'
    >>> repeterend(Breuk(13,22), compact=True)
    '5r90'
    """
    if maxbreedte is None: maxbreedte = MAXBREEDTE
    #bepaal plaats van de komma
    schuif = 0
    t, n = f.numerator, f.denominator
    t = abs(t)
    while t>=n:
        schuif +=1
        n *= GRONDTAL
    wijzers = {',': schuif}
    cijfers = ''
    bekendeResten = {}
    while t:
        d,t = divmod(t*GRONDTAL, n)
        cijfers += ALLECIJFERS[d]
        if t in bekendeResten:
            #we zijn rond
            #kort het vaste deel in als repeterende cijfers gelijk zijn
            repLen = len(cijfers)-bekendeResten[t]
            while len(cijfers)>repLen \
                    and cijfers[-1]==cijfers[-1-repLen]:
                cijfers = cijfers[:-1]
            #zet het symbool op de goede plaats
            wijzers['r'] = len(cijfers)-repLen
            break
        #als er nog ruimte is: onthou rest voor rep detectie
        #anders hoef je niks meer op te slaan
        if len(cijfers)<=maxbreedte: bekendeResten[t] = len(cijfers)
    assert max(wijzers.values())<=maxbreedte
    #controleer of resultaat lang genoeg, zet er anders cijfers bij
    while max(wijzers.values())>len(cijfers):
        #als de berekening is afgebroken, worden onzin cijfers toegevoegd
        #maar die worden er straks weer afgeknipt
        if 'r' in wijzers:
            cijfers += cijfers[-repLen]
            wijzers['r'] +=1
        else:
            cijfers += '0'
    #zet resultaat in elkaar
    if 'r' not in wijzers:
        #overbodige komma bij geheel getal
        if wijzers[',']==len(cijfers): del wijzers[',']
    elif compact:
        #als compact: impliciete komma bij repeterend getal
        if wijzers[',']==0: del wijzers[',']
    #negatief
    if f<0: wijzers['-'] = 0
    resultaat = plaatsWijzers(cijfers, wijzers)
    #knip op lengte, knipt ook onzincijfers eraf
    if len(resultaat)<=maxbreedte: return resultaat
    return resultaat[:maxbreedte-3]+'...'

#-------------------getaltheorie
def vastEnRepLen2(n):
    """Geef twee getallen a en b zodat a en b zo klein mogelijk en
    GRONDTAL**a en GRONDTAL**b-1 een veelvoud zijn van n
    Hier onder staat een snellere versie, die kennis van getaltheorie gebruikt
    """
    #zoek alle factoren in n die ook in GRONDTAL zitten
    t = n
    a = 0
    while True:
        g = gcd(t, GRONDTAL)
        if g==1: break
        a +=1
        if a>MAXBREEDTE: raise ValueError('Getal te moeilijk')
        t //= g
    else: raise ValueError('Getal te moeilijk')
    #zoek macht van grondtal die 1 meer is dan een veelvoud van de rest
    if t==1: return a,0
    b = 1
    r = GRONDTAL%t
    while r>1:
        b +=1
        if b>MAXBREEDTE: raise ValueError('Getal te moeilijk')
        r = r*GRONDTAL%t
        if r==t-1:
            #we zijn precies op de helft (gebeurt heel vaak!)
            b *=2
            break
    assert GRONDTAL**a*(GRONDTAL**b-1)%n==0
    return a,b

def delers():
    """Genereer getallen bruikbaar voor trial division
    Reeks met alle priemgetallen en weinig composiete getallen;
    de eerste zijn 49, 77, 91 (op 25 priemgetallen <100)
    """
    yield 2
    yield 3
    yield 5
    dertigtal = 0
    while dertigtal<MAXBREEDTE:
        yield dertigtal+7
        yield dertigtal+11
        yield dertigtal+13
        dertigtal += 30
        yield dertigtal-13
        yield dertigtal-11
        yield dertigtal-7
        yield dertigtal-1
        yield dertigtal+1
    raise NotImplementedError

def factorisatie(n, start=None):
    """Produceer factorisatie in een defaultdict {p:r}
    als je start opgeeft, gaat hij verder daarin
    Gebruikt alleen trial division.
    >>> factorisatie(362800)
    defaultdict(<class 'int'>, {2: 4, 5: 2, 907: 1})
    >>> reduce(mul, starmap(pow, factorisatie(425984).items()))
    425984
    """
    if start is None: resultaat = defaultdict(int)
    else: resultaat = start
    delerGen = delers()
    while True:
        d = next(delerGen)
        while n%d==0:
            n //= d
            resultaat[d] +=1
        if d*d>=n: break
    if n>1: resultaat[n] +=1
    return resultaat

def carmichael(fn):
    """Produceer de carmichaelfunctie van een ontbonden getal (als dict),
    resultaat is ook ontbonden in factoren
    (Niet compatibel met numtheory)
    >>> carmichael({5:1, 7:1})
    defaultdict(<class 'int'>, {2: 2, 3: 1})
    """
    resultaat = 1
    priemFactoren = set()
    for p,r in fn.items():
        if not r: continue
        #bepaal de grootste gemene deler met resultaat
        pr = p**(r-1)*(p-1)
        resultaat *= pr//gcd(resultaat, p**(r-1)*(p-1))
        if r>1: priemFactoren.add(p)
    ontbonden = defaultdict(int)
    #doe eerst de mogelijk grotere priemfactoren die we zeker weten
    for p in priemFactoren:
        while resultaat%p==0:
            resultaat //= p
            ontbonden[p] +=1
    #er zitten mogelijk nog wat kleinere factoren in
    return factorisatie(resultaat, ontbonden)

def vastEnRepLen(n, maxbreedte=None):
    """Geef twee getallen a en b zodat a en b zo klein mogelijk en
    GRONDTAL**a en GRONDTAL**b-1 een veelvoud zijn van n
    Dit is de snelle versie, die gebruik maakt van wat getaltheorie
    >>> vastEnRepLen(65)
    (1, 6)
    """
    pr = factorisatie(n)
    a = max(
        (pr[p]-1)//r+1
        for p,r in factorisatie(GRONDTAL).items()
        )
    #haal de factoren van het grondtal eruit
    rest = n//gcd(n, GRONDTAL**a)
    if rest==1: return a,0
    #we weten dat b een deler is van carmichael(rest)
    prc = carmichael(pr)
    b = reduce(mul, starmap(pow, prc.items()), 1)
    for p in prc:
        #kijk of het toevallig b//p is, dan is het korter
        while b%p==0 and pow(GRONDTAL, b//p, rest)==1: b //= p
    if b>(maxbreedte or MAXBREEDTE): raise ValueError("Te moeilijk")
    return a, b

# use internal invmod if available
try:
    pow(3, -1, 5) == 2
except TypeError:
    def invmod(x,n):
        """Geef inverse van x mod n
        """
        ar,br=x%n,n
        aa,ba=1,0 #invariant: ar=aa*x+?*n;br=ba*x+?*n
        while br!=0:
            q=ar//br
            ar,br=br,ar-q*br
            aa,ba=ba,aa-q*ba
        #we weten dat ar=aa*x+?*n=aa*x (mod n)
        assert ar==1
        #nu weten we dat aa*x==1 mod n
        return aa%n
else:
    def invmod(x, n):
        return pow(x, -1, n)

def quote(f, maxbreedte=None, compact=False):
    """Snellere versie van quote, die de deling in een klap doet.
    maxbreedte is de maximale breedte van de uitvoer
    compact genereert een ! in plaats van ',
    >>> quote(Breuk(1, 7))
    "285714'3"
    >>> quote(Breuk(3, 26))
    "384615',5"
    >>> quote(Breuk(26,25))
    '1,04'
    >>> quote(Breuk(355,113), 25)
    "...05309734513274336283'5"
    """
    #alle berekeningen zijn in integers!
    t,n = f.numerator, f.denominator
    a,b = vastEnRepLen(n)
    if max(a, b)>MAXBREEDTE:
        #getal is zo breed dat het overflow dreigt te veroorzaken
        #gedeelte voor de komma komt niet eens in beeld
        if a>maxbreedte-4: return '.%0d'%(t*10**(maxbreedte-4)//n)
        wijzers = {}
        if a:
            #er moet nog een komma tussen: pas getal en breedte aan
            f *= 10**a
            t, n = f.numerator, f.denominator
            maxbreedte -= 1
            wijzers = {',': maxbreedte+3-a}
        modulus = 10**(maxbreedte-3)
        #bouw resultaat op
        return plaatsWijzers(
            '...%0*d'%(maxbreedte, t*invmod(n, modulus)%modulus),
            wijzers)
    A,B = GRONDTAL**a, GRONDTAL**b-1
    #los de vergelijking c/A-d/B = f oftewel c*B-d*A=t*A*B//n op
    #met d=repDeel zo klein mogelijk >=0
    if B:
        repDeel = (invmod(-A, B)*t*A*B//n)%B
        vastDeel = (t*A*B//n+repDeel*A)//B
        #op dit punt is repDeel het repeterende deel van de breuk
        #en vastDeel het vaste deel, opgeschoven om het geheel te maken
        assert Breuk(vastDeel,A)-Breuk(repDeel,B)==f
    else:
        vastDeel = t*A//n
        #om te zorgen dat de overlap en redundatiecheck goed gaat
        b, B = 1, 9
        if f>=0: repDeel = 0
        else:
            #geef negatieve aan met een heleboel negens
            vastDeel, repDeel = vastDeel+A, 9
    #het getal bestaal uit <repdeel>'<overlap>,<vastDeel mod A>
    #waarbij overlap is een aantal keren repDeel, plus vastDeel//A
    overlap, overlapLengte = 0,0
    #verleng overlap tot het genoeg is
    while not 0<=overlap+vastDeel//A<GRONDTAL**overlapLengte:
        #voeg een repDeel aan overlap toe
        overlap = overlap*(B+1)+repDeel
        overlapLengte += b
    #zet getal in elkaar
    cijfers = talstelsel(repDeel, b)
    cijfers += talstelsel(overlap+vastDeel//A, overlapLengte)
    cijfers += talstelsel(vastDeel%A, a)
    #haal overbodige cijfers weg
    redundant = 0
    while len(cijfers)>b+redundant and cijfers[redundant]==cijfers[b+redundant]:
        redundant +=1
    wijzers = {}
    wijzers = {"'": b}
    if repDeel==0:
        redundant = 1
        del wijzers["'"]
    if a: wijzers[","] = b+overlapLengte-redundant
    resultaat = plaatsWijzers(cijfers[redundant:], wijzers)
    #gebruik ! bij compact
    if compact: resultaat = resultaat.replace("',", '!')
    if maxbreedte is None or len(resultaat)<=maxbreedte: return resultaat
    return '...'+resultaat[-maxbreedte+3:]

def repeterend(f, maxbreedte=None, compact=False):
    """Schrijf f als repeterende breuk, in 1 stap.
    Zet maxbreedte op het maximaal aantal tekens uitvoer dat je wil.
    compact laat de komma aan het begin we als dat kan
    >>> repeterend(Breuk(1,3))
    ',r3'
    >>> repeterend(Breuk(-100,3))
    '-3r3,'
    >>> repeterend(Breuk(-1,40))
    '-,025'
    >>> repeterend(Breuk(13,22), compact=True)
    '5r90'
    """
    #alle berekeningen zijn in integers!
    t,n = abs(f.numerator), f.denominator
    a,b = vastEnRepLen(n)
    A,B = GRONDTAL**a, GRONDTAL**b-1
    #schrijf f als (c+d/B)/A
    vastDeel = t*A//n
    #repDeel = (f-vastDeel/A))*A = (t*A*B/n-vastDeel*B)/B
    if B:
        repDeel = t*A*B//n-vastDeel*B
        #op dit punt is repDeel het repeterende deel van de breuk
        #en vastDeel het vaste deel, opgeschoven om het geheel te maken
        assert Breuk(vastDeel,A)+Breuk(repDeel,A*B)==abs(f)
    else:
        repDeel = 0
        #om te zorgen dat de checks goed gaan
        b = 1
    #zet getal in elkaar
    cijfers = talstelsel(vastDeel, a, expandeer=True)
    wijzers = {}
    wijzers[','] = len(cijfers)-a
    if B:
        cijfers += talstelsel(repDeel, b)
        minlen = max(wijzers[','], b)
        while len(cijfers)>minlen and cijfers[-1]==cijfers[-b-1]:
            cijfers = cijfers[:-1]
        wijzers['r'] = len(cijfers)-b
        if compact and wijzers[',']==0: del wijzers[',']
    elif wijzers[',']==len(cijfers): del wijzers[',']
    if f<0: wijzers['-'] = 0
    resultaat = plaatsWijzers(cijfers, wijzers)
    #knip op lengte, knipt ook onzincijfers eraf
    if maxbreedte is None or len(resultaat)<=maxbreedte: return resultaat
    return resultaat[:maxbreedte-3]+'...'

#------------------leuk
def cijfersZelfWortel(eindCijfer):
    """Generereer de cijfers van het getal dat op 5 of 6 eindigt
    en gelijk is aan zijn eigen kwadraat.
    Methode:
    Bereken het kwadraat van de output cijfer voor cijfer.
    Sla steeds het huidige cijfer over in de berekening,
    bepaal dit nadat je de berekening hebt gedaan.
    Voor 5 is dit het eindCijfer van de berekening
    (en dan gaat er een tienvoud bij, wat het eindCijfer niet verandert)
    voor 6 is dit 10 minus het eindCijfer
    (waarna het eindCijfer "omklapt")
    >>> from itertools import islice
    >>> list(islice(cijfersZelfWortel(5), 10))[::-1]
    [8, 2, 1, 2, 8, 9, 0, 6, 2, 5]
    >>> list(islice(cijfersZelfWortel(6), 10))[::-1]
    [1, 7, 8, 7, 1, 0, 9, 3, 7, 6]
    >>> import os,sys
    >>> sys.path.append(os.path.join(os.getcwd(), '../bitbucket'))
    >>> #direct method, assuming invmod is available:
    >>> 2**20*invmod(2**20,5**20)
    7743740081787109376
    >>> 5**20*invmod(5**20,2**20)
    92256259918212890625
    """
    if eindCijfer not in (5,6): raise ValueError
    yield eindCijfer
    #resultaat bevat alle cijfers behalve het eindCijfer
    resultaat = []
    cijferSom = eindCijfer**2
    while True:
        #overdracht van vorige
        cijferSom //= 10
        #bereken volgende cijfer, met carry van vorige
        #volgende cijfer wordt nog niet meegenomen in uitkomst
        #(want eindCijfer ontbreekt in resultaat)
        cijferSom += sum(map(mul, resultaat, reversed(resultaat)))
        #bepaal volgende cijfer
        nieuwCijfer = cijferSom if eindCijfer==5 else -cijferSom
        nieuwCijfer %= 10
        yield nieuwCijfer
        #vul cijfersom aan met effect van het eindCijfer
        cijferSom += 2*nieuwCijfer*eindCijfer
        #voeg cijfer toe aan resultaat
        resultaat.append(nieuwCijfer)

def nulpunten(p, digits):
    """Vind nulpunten van een gegeven polynoom.
    Het polynoom krijgt twee waarden: x en de modulus
    De modulus kan worden gebruikt om de snelheid op te voeren
    (met name in pow(x, n, m) natuurlijk)
    >>> #gewoon delen
    >>> nulpunten(lambda x,m:x*7-1, 20)
    [42857142857142857143]
    >>> #idempotenten
    >>> nulpunten(lambda x,m:pow(x,2,m)-x, 10)
    [0, 1, 8212890625, 1787109376]
    >>> #wortels uit 1
    >>> nulpunten(lambda x,m:pow(x,2,m)-1, 21)
    [1, 215487480163574218751, 784512519836425781249, 999999999999999999999]
    >>> #de derdemachtswortel uit 3 (er is er maar 1...)
    >>> #(er zijn er uit 3, 7, 8, 9, 11, 13, 17, 19, 21, 23, 24, ...)
    >>> nulpunten(lambda x,m:pow(x,3,m)-3,50)
    [78683312291648481630318492665160423850087895134587]
    >>> #vijfdemachtswortels: 7, 32, 43, 49, 51, 57, 93, 99
    """
    m = 1
    solutions = set(range(10))
    for d in range(digits+1):
        prevMod = m
        m = prevMod*10
        #the values of solutions extended by one digit
        newSolutions = set()
        #the solutions that were expandable by another digit
        activeSolutions = set()
        for s in solutions:
            for ns in range(s, m, prevMod):
                if p(ns, m)%m==0:
                    newSolutions.add(ns)
                    activeSolutions.add(s)
        solutions = newSolutions
        if len(activeSolutions)>1000: raise ValueError("Teveel oplossingen")
    #only return numbers that are potentially p-adic
    #sort in 'p-adic order'
    return sorted(activeSolutions, key=lambda s: ('%*d'%(digits, s))[::-1])

#------------------hoofdprogramma
__test__ = {
    'Zweet': '''
        >>> for t in range(25):
        ...   for n in range(1,t):
        ...     if Breuk(n,t).numerator!=n: continue
        ...     r = repeterend(Breuk(n,t))
        ...     assert bereken(r)==Breuk(n,t), str((r, Breuk(n, t)))
    ''',
    'maxlen': '''
        >>> repeterend(Breuk(1, 188))
        ',00r5319148936170212765957446808510638297872340425'
        >>> repeterend(Breuk(1, 188), 40)
        ',00r531914893617021276595744680851063...'
        >>> quote(Breuk(1, 188))
        "446808510638297872340425531914893617021276595,7'5"
        >>> quote(Breuk(1,188), 100)
        "446808510638297872340425531914893617021276595,7'5"
        >>> quote(Breuk(1,188), 40)
        "...297872340425531914893617021276595,7'5"
    ''',
    'grondtal': '''
        >>> import padisch; padisch.GRONDTAL = 2
        >>> try:
        ...    padisch.quote(padisch.bereken('/101'))
        ... finally: padisch.GRONDTAL = 10
        "0110'1"
    ''',
    'faal': '''
        >>> for s in (
        ...    "1/7",
        ...    "3/26",
        ...    "26/25",
        ...    "1/3",
        ...    "1/4",
        ...    "13/22",
        ...    "-100/3",
        ...    "-1/40",
        ...    "3/4+67'22.1",
        ...       ):
        ...    f = bereken(s)
        ...    if quote(f)!=quote2(f): print('quote ', s, quote(f), quote2(f))
        ...    if repeterend(f)!=repeterend2(f): print('rep ', s, repeterend(f), repeterend2(f))
    '''
}

def voorbeelden():
    "geef wat random voorbeelden van breuken"
    import random
    voorbeelden = []
    for i in range(10000):
        a,b = random.randrange(1,1000),random.randrange(1,1000)
        if len(str((a,b,quote(Breuk(a,b)))))<20:
            f = Breuk(a,b)
            voorbeelden.append((f,repeterend(f),quote(f),quote(-f)))
            assert bereken(repeterend(f))==bereken(quote(f))==f
            assert bereken(repeterend(-f))==bereken(quote(-f))==-f
            if len(voorbeelden)>15: break
    print('\n'.join(map(str,
    	  sorted(voorbeelden,
    	  key=lambda t:len(str(t[:2])))
    	  )))

def periode(d):
    """Bepaal periode van getal met deler d.
    >>> [periode(n) for n in (3, 7, 9, 11, 13, 17, 19, 21, 23, 27, 29)]
    [1, 6, 1, 2, 6, 16, 18, 6, 22, 3, 28]
    >>> [periode(n) for n in range(1,15)]
    [0, 0, 1, 0, 0, 1, 6, 0, 1, 0, 2, 1, 6, 6]
    """
    while gcd(d, GRONDTAL)>1: d //= gcd(d, GRONDTAL)
    if d==1: return 0
    t = 1
    for i in range(1,d):
        t = t*GRONDTAL%d
        if t==1: return i

def zoekPeriode(n):
    """Genereer delers die een gegeven repetitielengte hebben.
    Pas op: heel langzaam!
    >>> [zoekPeriode(n) for n in range(1,10)]
    [3, 11, 27, 101, 41, 7, 239, 73, 81]
    """
    r = 1
    while periode(r)!=n:
        while True:
            r +=1
            if gcd(r, GRONDTAL)==1: break
    return r

if __name__=='__main__':
    import doctest
    doctest.testmod()
    #het demoprogramma: lees expressies en bereken de uitkomst
    breedte = 68
    getcontext().prec = breedte
    while True:
        parser = Parser()
        try:
            parser.verwerkString(input('Geef expressie: '))
            resultaat = parser.uitkomst()
        except SyntaxError as msg:
            print("Sorry, dit snap ik niet:")
            print(msg.args[0])
            continue
        except ValueError as msg:
            print("Sorry, ik kan dit niet berekenen: "+msg.args[0])
            continue
        except EOFError: break
        print('ongeveer:', Decimal(resultaat.numerator)/resultaat.denominator)
        print('p-adisch:', quote(resultaat, breedte, COMPACT))
        print('decimaal:', repeterend(resultaat, breedte, COMPACT))
        print('breuk:   ', str(resultaat))
        print()
        Laatste.waarde = resultaat
