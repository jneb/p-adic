# P-adic #

P-adic numbers are numbers which have an infinite number of digits **before** the comma.
These mind boggling weird numbers can be used for calculations with fractions,
just like repeating decimals.

I wrote this program to investigate the properties of these numbers.

This program is a calculator. It can do calculations with fractions,
representing the result as p-adic numbers with base 10, repeating decimals, and fractions.
You can also enter p-adic numbers and repeating decimals in the computations.
(This commit is in Dutch, translation may follow if you are interested).

## Usage ##

* You can use this as a calculator, but the most interesting part is the algorithms,
  of course.
  The point of the program was to document the efficient algorithms.
* The program runs from Python 3.3 up, and it will use the new modular inverse in 3.8
* There is internal support for other number bases, but it isn't implemented on the interface yet

### Supported operators:
* \+ \- \* / what you expect
* % is modulus
* \- / also are prefix operators, so /n stands for 1/n like -n stands for 0-n
* ^ is the power operator (note that only powers that give a fraction will work)

### Ways to write numbers:
(where \# is any digit)

* ##.##r## Use the r to indicate that digits after is are repeated
* ##'## Use single quote to indicate that digits before it are to be repeated (p-adic numbers)
* ! is a shorthand for '.
* _ gives you the previous result
* ~ before a number approximates it as p-adic: : ~3 = -1/3
* ~ behind a number approximates it with a fraction: .14~ = 1/7
* Note that is isn't a requirement to put r after the period, nor ' before it:
    142.857'2 = 1/17500; r2.7 = 30/11
* If you use an r without comma, it is implied to be on the left of the digits:
    r6 = 2/3
* Program outputs a comma instead of a period for decimals, but it will accept both

## Example ###

    Geef expressie: /7
    ongeveer: 0.14285714285714285714285714285714285714285714285714285714285714285714
    p-adisch: 285714'3
    decimaal: r142857
    breuk:    1/7
      
    Geef expressie: .09'
    ongeveer: -0.00090909090909090909090909090909090909090909090909090909090909090909091
    p-adisch: ,09'
    decimaal: -00r09
    breuk:    -1/1100
      
    Geef expressie: 3+(34/.7)-/3
    ongeveer: 51.238095238095238095238095238095238095238095238095238095238095238095
    p-adisch: 047619'56
    decimaal: 51,r238095
    breuk:    1076/21

    Geef expressie: .1429~
    ongeveer: 0.14285714285714285714285714285714285714285714285714285714285714285714
    p-adisch: 285714'3
    decimaal: r142857
    breuk:    1/7

## Contribution guidelines ##

* I would really like to hear if you like or use this program!

### Known bugs ###
* The program speaks Dutch; this should be English (op optional) for it to be more useful
* The program does not give builtin help
* Although the program is written to support p not equal to 10,
  it isn't implemented in the interface
* Although the program supports ridiculously long repeating parts,
  the interface doesn't even show the length
* ~6 doesn't work, while ~66 does
